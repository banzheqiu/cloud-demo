package cn.itcast.gateway.config;

import lombok.Data;
import lombok.extern.slf4j.Slf4j;
import org.springframework.cloud.gateway.filter.GatewayFilter;
import org.springframework.cloud.gateway.filter.GatewayFilterChain;
import org.springframework.cloud.gateway.filter.factory.AbstractGatewayFilterFactory;
import org.springframework.stereotype.Component;
import org.springframework.web.server.ServerWebExchange;
import reactor.core.publisher.Mono;

import java.util.Arrays;
import java.util.List;


/**
 * 参考AuthCheckTokenGatewayFilterFactory
 */
@Slf4j
@Component
public class MyGatewayFilterFactory extends AbstractGatewayFilterFactory<MyGatewayFilterFactory.Config> {

    /**
     * 不能少！
     */
    public MyGatewayFilterFactory() {
        super(MyGatewayFilterFactory.Config.class);
    }


    /**
     * 使用匿名内部类
     *
     * @param config
     * @return
     */
    @Override
    public GatewayFilter apply(Config config) {
        return new GatewayFilter() {
            @Override
            public Mono<Void> filter(ServerWebExchange exchange, GatewayFilterChain chain) {
                String status = config.getStatus();
                log.info("----- MyGatewayFilterFactory # status:{}", status);
                return chain.filter(exchange);
            }
        };
    }


    /**
     * 此处和Config中的字段名称对应上~
     *
     * @return List<String>
     */
    @Override
    public List<String> shortcutFieldOrder() {
        return Arrays.asList("status");
    }

    @Data
    public static class Config {
        private String status;
    }
}
