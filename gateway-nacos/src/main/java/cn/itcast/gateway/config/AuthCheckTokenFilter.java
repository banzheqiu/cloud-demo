package cn.itcast.gateway.config;

import lombok.extern.slf4j.Slf4j;
import org.springframework.cloud.gateway.filter.GatewayFilter;
import org.springframework.cloud.gateway.filter.GatewayFilterChain;
import org.springframework.cloud.gateway.filter.GlobalFilter;
import org.springframework.core.Ordered;
import org.springframework.web.server.ServerWebExchange;
import reactor.core.publisher.Mono;

import java.util.List;

@Slf4j
public class AuthCheckTokenFilter implements GatewayFilter, Ordered {

    /**
     * 根据构造方法传入的config配置，进行过滤
     *
     * @param exchange exchange
     * @param chain
     * @return
     */
    @Override
    public Mono<Void> filter(ServerWebExchange exchange, GatewayFilterChain chain) {
        List<String> noFilterUrl = config.getNoFilterUrl();
        log.info("AuthCheckTokenFilter#noFilterUrl:{}", noFilterUrl);
        return chain.filter(exchange);
    }

    @Override
    public int getOrder() {
        return 0;
    }


    private AuthCheckTokenGatewayFilterFactory.Config config;

    /**
     * 构造方法设置值
     */
    public AuthCheckTokenFilter(AuthCheckTokenGatewayFilterFactory.Config config) {
        this.config = config;

    }
}
