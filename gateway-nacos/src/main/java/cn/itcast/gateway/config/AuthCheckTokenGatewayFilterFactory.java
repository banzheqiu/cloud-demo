package cn.itcast.gateway.config;

import lombok.Data;
import org.springframework.cloud.gateway.filter.GatewayFilter;
import org.springframework.cloud.gateway.filter.factory.AbstractGatewayFilterFactory;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;


/**
 * 参考：SetPathGatewayFilterFactory
 */
@Component
public class AuthCheckTokenGatewayFilterFactory extends AbstractGatewayFilterFactory<AuthCheckTokenGatewayFilterFactory.Config> {


    /**
     * 不能少~~
     */
    public AuthCheckTokenGatewayFilterFactory() {
        super(AuthCheckTokenGatewayFilterFactory.Config.class);
    }


    /**
     * 将此配置类config作为AuthCheckTokenFilter的构造函数的参数
     *
     * @param config config
     * @return 返回一个自定义的 AuthCheckTokenFilter
     */
    @Override
    public GatewayFilter apply(Config config) {
        return new AuthCheckTokenFilter(config);
    }

    //配置类，对应yaml中的配置
//  - name: AuthCheckToken  # 对应~AuthCheckTokenGatewayFilterFactory
//    args:
//    noFilterUrl:
//            - /user/login
//            - /user/logout
    @Data
    public static class Config {

        List<String> noFilterUrl = new ArrayList<>();
    }
}
