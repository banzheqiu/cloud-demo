package cn.itcast.order.web;

import cn.itcast.order.pojo.Order;
import cn.itcast.order.service.OrderService;
import com.alibaba.csp.sentinel.annotation.SentinelResource;
import com.alibaba.csp.sentinel.slots.block.BlockException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.UUID;

@RefreshScope
@RestController
@RequestMapping("/order")
public class OrderController {

    @Autowired
    private OrderService orderService;


    @Value("${order.param1}")
    private String param1;

    @Value("${order.param2}")
    private String param2;


    /**
     * 限流参数：
     * value=资源名
     * blockHandler=兜底方法名
     * blockHandlerClass=兜底方法所在类（ 与资源方法在同一个类中 这属性可以省去）
     *
     * @param orderId
     * @return
     */
    @SentinelResource(value = "queryOrderByUserIdByFeign", blockHandler = "fallbackHandler", blockHandlerClass = OrderController.class)
    @GetMapping("/feign/{orderId}")
    public Order queryOrderByUserIdByFeign(@PathVariable("orderId") Long orderId) {
        return orderService.queryOrderByUserIdByFeign(orderId);
    }


    @GetMapping("/restTemplate/{orderId}")
    public Order queryOrderByUserIdByRestTemplate(@PathVariable("orderId") Long orderId) {
        return orderService.queryOrderByUserIdByRestTemplate(orderId);
    }


    //优先级[ 优先级从高到低]：
    // order-service-dev.yml
    // > order-service.yml
    // > application-dev.yml
    // >application.yml
    // > bootstrap.yml
    // 优先级高的覆盖优先级低的
    @GetMapping("/config")
    public String config() {
        return "param1:" + param1 + "\n" +
                "param2:" + param2;
    }

    public static String fallbackHandler(Long orderId) {
        String msg = "不好意思，前方拥挤，请您稍后再试";
        return msg;
    }


}
