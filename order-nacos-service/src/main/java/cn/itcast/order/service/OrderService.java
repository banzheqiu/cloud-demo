package cn.itcast.order.service;

import cn.itcast.feign.clients.UserClient;
import cn.itcast.feign.pojo.User;
import cn.itcast.order.pojo.Order;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

@Service
public class OrderService {


    @Autowired
    private RestTemplate restTemplate;

    @Autowired
    private UserClient userClient;

    public Order queryOrderByUserIdByFeign(Long id) {
        Order order = new Order();
        order.setId(id);
        order.setUserId(id);
        //使用FeignClient发起http请求，查询用户
        User user = userClient.findById(order.getUserId());
        order.setUser(user);
        return order;
    }

    public Order queryOrderByUserIdByRestTemplate(Long id) {
        Order order = new Order();
        order.setId(id);
        order.setUserId(id);
        //利用RestTemplate发起http请求，查询用户
        String restUrl = "http://localhost:8081/user/" + id;
        String registerUrl = "http://user-service/user/" + id;
        User user = restTemplate.getForObject(registerUrl, User.class);
        order.setUser(user);
        return order;
    }
}
