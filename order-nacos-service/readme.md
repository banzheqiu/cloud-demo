微服务启动时从nacos读取多个配置文件
[spring.application.name]-[spring.profiles.active].yaml  例如：user-service-dev.yaml
[spring.application.name].yaml 例如 user-service.yaml
优先级由高到低：服务名-profile.yaml > 服务名.yaml  > 本地配置



nacos优先级：优先级由高到低：服务名-profile.yaml > 服务名.yaml  > 本地配置
举例：
//优先级[ 优先级从高到低]：
// order-service-dev.yml
// > order-service.yml
// > application-dev.yml
// >application.yml
// > bootstrap.yml
// 优先级高的覆盖优先级低的


启动sentinel
java -Dserver.port=8080 -Dcsp.sentinel.dashboard.server=localhost:8080 -Dproject.name=sentinel-dashboard -jar sentinel-dashboard-1.8.8.jar