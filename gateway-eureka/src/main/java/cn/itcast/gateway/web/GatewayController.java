package cn.itcast.gateway.web;

import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.time.LocalDateTime;

@RefreshScope
@RestController
@RequestMapping("/gateway")
public class GatewayController {
    @GetMapping("/test")
    public String test() {
        return String.valueOf(LocalDateTime.now());


    }
}
