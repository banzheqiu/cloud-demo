package cn.itcast.gateway;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;


@EnableEurekaClient
@SpringBootApplication(scanBasePackages = "cn.itcast")
public class GatewayEurekaApplication {
    public static void main(String[] args) {
        SpringApplication.run(GatewayEurekaApplication.class, args);
    }
}
