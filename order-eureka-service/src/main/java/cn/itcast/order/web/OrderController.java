package cn.itcast.order.web;

import cn.itcast.order.pojo.Order;
import cn.itcast.order.service.OrderService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RefreshScope
@RestController
@RequestMapping("/order")
public class OrderController {

    @Autowired
    private OrderService orderService;


    @Value("${order.param1}")
    private String param1;

    @Value("${order.param2}")
    private String param2;

    @GetMapping("/feign/{orderId}")
    public Order queryOrderByUserIdByFeign(@PathVariable("orderId") Long orderId) {
        return orderService.queryOrderByUserIdByFeign(orderId);
    }


    @GetMapping("/restTemplate/{orderId}")
    public Order queryOrderByUserIdByRestTemplate(@PathVariable("orderId") Long orderId) {
        return orderService.queryOrderByUserIdByRestTemplate(orderId);
    }


    @GetMapping("/config")
    public String config() {
        return "param1:" + param1 + "\n" +
                "param2:" + param2;
    }


}
