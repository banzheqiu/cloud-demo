package cn.itcast.order;


import com.netflix.loadbalancer.IRule;
import com.netflix.loadbalancer.RandomRule;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.loadbalancer.LoadBalanced;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;
import org.springframework.cloud.openfeign.EnableFeignClients;
import org.springframework.context.annotation.Bean;
import org.springframework.web.client.RestTemplate;



@EnableEurekaClient
@SpringBootApplication
@EnableFeignClients(basePackages = "cn.itcast.feign.clients")
public class OrderEurekaApplication {

    public static void main(String[] args) {
        SpringApplication.run(OrderEurekaApplication.class, args);
    }

    /**
     * 创建RestTemplate并注入Spring容器
     */
    @Bean
    @LoadBalanced
    public RestTemplate restTemplate() {
        return new RestTemplate();
    }

    /*
     * 定义负载均衡的方案
     *
     * @return IRule
     */
    @Bean
    public IRule randomRule() {
        return new RandomRule();
    }
}